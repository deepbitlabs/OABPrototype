﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour 
{
	public enum EnemyState {Idle, Walking, Attacking, BeingHit, Waiting, Dead, Alive }
	public EnemyState state = EnemyState.Walking;
	public float attackCooldownTime = 1f;
	private float attackCooldownTimeOut = 0f;
	public float despawnTime = 2f;
	private float despawnTimeOut = 0f;
	private EnemyController enemyController;
	private Animator myAnimator;
	// Use this for initialization
	void Start () {
		enemyController = GetComponent<EnemyController>();
		myAnimator = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		switch (state)
		{
		case EnemyState.Idle: break;
		case EnemyState.Walking: UpdateWalking(); break;
		case EnemyState.Attacking: UpdateAttacking(); break;
		case EnemyState.Waiting: UpdateWaiting(); break;
		case EnemyState.BeingHit: UpdateBeingHit(); break;
		case EnemyState.Dead: UpdateBeingDead(); break;
            //case EnemyState.Alive: UpdateBeingAlive(); break;
		}
	}

	private void TransitionToWalking()
	{
		enemyController.FinishRigidMotionAnimation();
		state = EnemyState.Walking;
	}

	private void UpdateWalking()
	{
		enemyController.UpdateDestination();
		enemyController.UpdateAnimator();

		if (enemyController.IsAtDestination())
		{
			TransitionToAttacking();
		}
	}

	private void TransitionToAttacking()
	{
		state = EnemyState.Attacking;
		enemyController.StartRigidMotionAnimation();
		enemyController.ExecuteAttack();
	}

	private void UpdateAttacking()
	{
		//if (myAnimator.IsInTransition(0) && myAnimator.GetNextAnimatorStateInfo(0).IsName("Idle"))
		//{
		if(!myAnimator.IsInTransition(0) && myAnimator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
		{
			TransitionToWalking();
		}
	}

	private void TransitionToWaiting()
	{
		attackCooldownTimeOut = 0f;
		state = EnemyState.Waiting;
	}

	private void UpdateWaiting()
	{
		attackCooldownTimeOut += Time.deltaTime;
		if (attackCooldownTimeOut >= attackCooldownTime)
		{
			TransitionToAttacking();
		}
	}

	private void TransitionToBingHit()
	{
		enemyController.StartRigidMotionAnimation();
		state = EnemyState.BeingHit;
	}

	private void UpdateBeingHit()
	{
		if (!myAnimator.IsInTransition(0) && myAnimator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
		{
			TransitionToWalking();
		}
	}

	private void TransitionToBeingDead()
	{
		state = EnemyState.Dead;
		despawnTimeOut = 0;
	}

	private void UpdateBeingDead()
	{
		despawnTimeOut += Time.deltaTime;
		if (despawnTimeOut >= despawnTime)
		{
			Destroy(this.gameObject);
		}
	}

	public void OnHit()
	{
		TransitionToBingHit();
	}

	public void OnDeath()
	{
		TransitionToBeingDead();
	}
}
