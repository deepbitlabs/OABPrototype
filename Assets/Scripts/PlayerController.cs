﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float maxHealth = 100f;
    public float curHealth;

    

    bool isDead;

    void Start()
    {
        curHealth = maxHealth;
    }

    void Update()
    {
        if (!isDead)
        {
            if (curHealth <= 0)
            {
                killPlayer();
            }
        }
    }

    void killPlayer()
    {
        Debug.Log("Player is dead!");
    }
}
