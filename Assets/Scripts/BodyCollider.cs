﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyCollider : MonoBehaviour 
{
    //public EnemyController enemy;

    public float force = 50f;

    public Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

	void OnCollisionEnter(Collision col)
	{

        if (col.gameObject.tag == "Weapon")
        {
            if (gameObject.tag == "Enemy")
            {
                //enemy.killEnemy();

                var magnitude = 5000;

                var force = transform.position - col.transform.position;

                force.Normalize();

                gameObject.GetComponent<Rigidbody>().AddForce(force * magnitude);
            }
        }
	}
}
