﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGenerator : MonoBehaviour
{
    public Vector3 spawnLocation;
    public GameObject box;

    void Start()
    {
        if (box != true)
        {
            Debug.Log("box not set");
            box = GameObject.CreatePrimitive(PrimitiveType.Cube);
        }
        if (box.GetComponent<Renderer>().enabled == false)
        {
            Debug.Log("box not rendered");
            box = GameObject.CreatePrimitive(PrimitiveType.Cube);
        }
        Createbox();
    }

    void Createbox()
    {
        spawnLocation = new Vector3(0, Random.value * 10, 0);
        Instantiate(box, spawnLocation, Quaternion.identity);
    }
}
