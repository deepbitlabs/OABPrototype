﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public Rigidbody rigidbody;

    void OnCollisionEnter()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * 500, ForceMode.Acceleration);
    }
}
